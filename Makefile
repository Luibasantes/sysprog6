all:
	gcc -o sysprog6 main.c mask.c mask.h

.PHONY: clean
clean:
	rm -f *.o core
