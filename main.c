#include <stdio.h>
#include <ctype.h>
#include "mask.h"

static int mask_owner, mask_group, mask_other;

int main() {
	char respuesta;
	char newline;
	//Lectura Owner
	do{
		printf("Permiso lectura para owner? [s/n] ");
		scanf("%c",&respuesta);
		scanf("%c",&newline);
		respuesta = tolower(respuesta);
	}while( ! (respuesta=='s' || respuesta=='n') );
	setPermiso(&mask_owner,'r',(respuesta=='s') ? SET : UNSET);

	//Escritura Owner
	do{
		printf("Permiso escritura para owner? [s/n] ");
		scanf("%c",&respuesta);
		scanf("%c",&newline);
		respuesta = tolower(respuesta);
	}while( ! (respuesta=='s' || respuesta=='n') );
	setPermiso(&mask_owner,'w',(respuesta=='s') ? SET : UNSET);

	//Ejecucion Owner
	do{
		printf("Permiso ejecución para owner? [s/n] ");
		scanf("%c",&respuesta);
		scanf("%c",&newline);
		respuesta = tolower(respuesta);
	}while( ! (respuesta=='s' || respuesta=='n') );
	setPermiso(&mask_owner,'x',(respuesta=='s') ? SET : UNSET);

	//Lectura Group
	do{
		printf("Permiso lectura para group? [s/n] ");
		scanf("%c",&respuesta);
		scanf("%c",&newline);
		respuesta = tolower(respuesta);
	}while( ! (respuesta=='s' || respuesta=='n') );
	setPermiso(&mask_group,'r',(respuesta=='s') ? SET : UNSET);

	//Escritura Group
	do{
		printf("Permiso escritura para group? [s/n] ");
		scanf("%c",&respuesta);
		scanf("%c",&newline);
		respuesta = tolower(respuesta);
	}while( ! (respuesta=='s' || respuesta=='n') );
	setPermiso(&mask_group,'w',(respuesta=='s') ? SET : UNSET);

	//Ejecucion Group
	do{
		printf("Permiso ejecución para group? [s/n] ");
		scanf("%c",&respuesta);
		scanf("%c",&newline);
		respuesta = tolower(respuesta);
	}while( ! (respuesta=='s' || respuesta=='n') );
	setPermiso(&mask_group,'x',(respuesta=='s') ? SET : UNSET);

	//Lectura Other
	do{
		printf("Permiso lectura para other? [s/n] ");
		scanf("%c",&respuesta);
		scanf("%c",&newline);
		respuesta = tolower(respuesta);
	}while( ! (respuesta=='s' || respuesta=='n') );
	setPermiso(&mask_other,'r',(respuesta=='s') ? SET : UNSET);

	//Escritura Other
	do{
		printf("Permiso escritura para other? [s/n] ");
		scanf("%c",&respuesta);
		scanf("%c",&newline);
		respuesta = tolower(respuesta);
	}while( ! (respuesta=='s' || respuesta=='n') );
	setPermiso(&mask_other,'w',(respuesta=='s') ? SET : UNSET);

	//Ejecucion Other
	do{
		printf("Permiso ejecución para other? [s/n] ");
		scanf("%c",&respuesta);
		scanf("%c",&newline);
		respuesta = tolower(respuesta);
	}while( ! (respuesta=='s' || respuesta=='n') );
	setPermiso(&mask_other,'x',(respuesta=='s') ? SET : UNSET);


	//Las mascaras transformadas a octal
	printf("\nLa Máscara de permisos es la siguiente: \n %o%o%o\n",mask_owner,mask_group,mask_other);
}
